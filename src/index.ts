import config from './config.json'
console.log(config.appName)
import snoowrap from 'snoowrap'
const r = new snoowrap({
  userAgent: config.user_agent,
  clientId: config.client_id,
  clientSecret: config.client_secret,
  refreshToken: config.refresh_token,
})
r.getHot().then(console.log)
